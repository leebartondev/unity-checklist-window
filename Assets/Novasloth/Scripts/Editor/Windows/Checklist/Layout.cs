using UnityEngine;
using UnityEditor;

// Lee Barton
namespace Novasloth {

    public static class Layout {

        /////////////////////////////////////////////////////////////////
        // F I E L D S
        /////////////////////////////////////////////////////////////////

        public static string TextField (string text) {
            return GUILayout.TextField(text, 300, GUILayout.MinWidth(200));
        }

        public static void LabelField (string text, bool strikethrough) {
            EditorGUILayout.LabelField(
                (strikethrough ? StrikeThrough(text) : text),
                EditorStyles.label
            );
        }

        /////////////////////////////////////////////////////////////////
        // B U T T O N S
        /////////////////////////////////////////////////////////////////

        public static bool DeleteButton () {
            return SmallButton("x");
        }

        public static bool AddButton () {
            return SmallButton("+");
        }

        public static bool SmallButton (string text) {
            return GUILayout.Button(text, EditorStyles.miniButton, GUILayout.MaxWidth(50));
        }

        /////////////////////////////////////////////////////////////////
        // H E L P E R   M E T H O D S
        /////////////////////////////////////////////////////////////////

        public static string StrikeThrough (string text) {
            string strike = "";

            foreach (char character in text) {
                strike = strike + character + '\u0336';
            }
            return strike;
        }
    }
}
